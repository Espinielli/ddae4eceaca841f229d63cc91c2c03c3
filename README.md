Use the range slider to change the degree of subdivision in this geodesic sphere.

The base shape, visible when subdivision is disabled, is one of the 5 regular convex polyhedra
(with triangularized faces for cube and dodecahedron), a.k.a. [Platonic solids][five]:

* tethrahedron
* hexahedron (cube)
* octahedron
* dodecahedron
* icosahedron


Built with a modified version of the [d3.geodesic plugin](https://github.com/d3/d3-plugins/tree/master/geodesic)
which adds all Platonic solids.

Adapted to use D3v4.

Evolved from my [previous block][prev] which was as well inspired by
Mike Bostock's [Geodesic Rainbow][rainbow].

[five]: <https://en.wikipedia.org/wiki/Platonic_solid> "Platonic solids"
[prev]: <https://bl.ocks.org/espinielli/4470c433a63e39489b6baa91fad0a610> "Geodesic Rainbow: tetrahedron and Icosahedron"
[rainbow]: <https://bl.ocks.org/mbostock/3057239> "Mike Bostock's Geodesic Rainbow"